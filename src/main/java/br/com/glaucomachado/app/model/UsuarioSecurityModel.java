package br.com.glaucomachado.app.model;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * 
 * @author Glauco Machado
 * Essa classe é usada pelo Spring para dar as devidas permissões para o usuário.
 */

public class UsuarioSecurityModel extends User {

	private static final long serialVersionUID = 1L;

	public UsuarioSecurityModel(String login, String senha, Boolean ativo,
			Collection<? extends GrantedAuthority> authorities) {
		super(login, senha, ativo, true, true, true, authorities);
	}
}