package br.com.glaucomachado.app.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.glaucomachado.app.entity.UsuarioEntity;

/**
* 
* @author Glauco Machado
* 
*/

public interface UsuarioRepository extends JpaRepository<UsuarioEntity, Long> {

	UsuarioEntity findByLogin(String login);
	
	UsuarioEntity findByCodigo(Long codigo);

}