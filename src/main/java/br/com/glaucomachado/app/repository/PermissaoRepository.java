package br.com.glaucomachado.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.glaucomachado.app.entity.GrupoEntity;
import br.com.glaucomachado.app.entity.PermissaoEntity;

/**
 * 
 * @author Glauco Machado
 * Essa Interface tem um método para consultar as permissões de um grupo.
 */

public interface PermissaoRepository extends JpaRepository<PermissaoEntity, Long> {

	List<PermissaoEntity> findByGruposIn(GrupoEntity grupoEntity);
}