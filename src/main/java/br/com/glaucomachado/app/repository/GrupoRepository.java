package br.com.glaucomachado.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.glaucomachado.app.entity.GrupoEntity;
import br.com.glaucomachado.app.entity.UsuarioEntity;

/**
 * 
 * @author Glauco Machado
 * Essa Interface tem um método para consultar os grupos de um determinado usuário.
 */

@Repository
public interface GrupoRepository extends JpaRepository<GrupoEntity, Long> {

	List<GrupoEntity> findByUsuariosIn(UsuarioEntity usuarioEntity);
	
	GrupoEntity findByCodigo(Long usuarioEntity);
}