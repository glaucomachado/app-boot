package br.com.glaucomachado.app.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.glaucomachado.app.entity.GrupoEntity;
import br.com.glaucomachado.app.model.GrupoModel;
import br.com.glaucomachado.app.repository.GrupoRepository;

/**
 * 
 * @author Glauco Machado
 * Essa classe tem um método que retorna todos os grupos cadastrados no Banco de Dados.
 */

@Service
@Transactional
public class GrupoService {

	@Autowired
	private GrupoRepository grupoRepository;

	/** CONSULA OS GRUPOS CADASTRADOS */
	@Transactional(readOnly = true)
	public List<GrupoModel> consultarGrupos() {

		List<GrupoModel> gruposModel = new ArrayList<GrupoModel>();

		/* CONSULTA TODOS OS GRUPOS */
		List<GrupoEntity> gruposEntity = this.grupoRepository.findAll();

		gruposEntity.forEach(grupo -> {
			gruposModel.add(new GrupoModel(grupo.getCodigo(), grupo.getDescricao()));
		});

		return gruposModel;
	}

}